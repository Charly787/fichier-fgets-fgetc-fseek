<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
<body>

    <?php
    
    // Afficher un chiffre sur deux du fichier Chiffres.txt
    // TANT QUE celui-ci n'est pas arrivé à la fin

    $source = fopen('Chiffres.txt','rb');

    while (!feof($source)) {
        echo fgets($source).'<br>';
        fgets($source);
    }

    ?>

</body>
</html>
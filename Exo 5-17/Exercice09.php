<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Exercice PHP</title>
</head>
<body>
        
    <?php

    // L'avantage de mettre filesize dans une variable, c'est que l'on peut considerer la fonction
    // comme un chiffre.
    // Qui dit chiffre, dit opération... Manipuler la variable pour n'afficher que : 
    // Le 29/01/2020, nous allons apprendre que

    // PHP peux manipuler des fichiers facilement.

    $source = fopen('notes.txt', "rb");
    $taille = filesize('notes.txt');
    $contents = fread($source, 87);
    echo nl2br($contents);



        ?>

</body>
</html>